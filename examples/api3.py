import hmac, base64, hashlib, urllib, time, requests

base = 'https://data.mtgox.com/api/2/'
key = ""
sec = ""

def request(get_or_post, path, inp={}):

    def sign(path, data):
        return hmac.new(base64.b64decode(bytes(sec, 'UTF-8')), 
            bytes(path+chr(0)+data, 'UTF-8'), hashlib.sha512)

    if get_or_post == 'get':
        get_data = urllib.parse.urlencode(inp)
        return requests.get(base+path+'?'+get_data)

    inp[u'tonce'] = str(int(time.time()*1e6))
    post_data = urllib.parse.urlencode(inp)
    headers = {
        'Content-Type': 'application/x-www-form-urlencoded',
        'Rest-Key': key,
        'Rest-Sign': base64.b64encode(sign(path, post_data).digest())
    }
    return requests.post(base+path, data = post_data, headers = headers)

# example: get
#r = request('get', 'BTCUSD/money/ticker_fast')
#print(r.text)

# example: post
#r = request('post', 'BTCUSD/money/info')
#print(r.text)

# example: post with params (use your own order ID here)
#data = {'order': 'd5111d57-c495-48a0-923d-59b729dba331', 'type':'ask'}
#r = request('post', 'BTCUSD/money/order/result', data)
#print(r.text)